FROM alpine:3.14.1

WORKDIR deves
EXPOSE 8080
# Install all packages
RUN apk add --no-cache npm git curl python3 py-pip openssl ca-certificates bash nano wget unzip tmux openssh
RUN mkdir "tools" && \
  echo 'Installing ngrok' && \
  # Install ngrok
  wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && unzip ngrok-stable-linux-amd64.zip && rm ngrok-stable-linux-amd64.zip && ./ngrok version && \
  mv ./ngrok ./tools/ngrok && \
  echo 'done.'

COPY . .
RUN ./scripts/internal/fix-basescript.sh
ENTRYPOINT ["./scripts/docker-entry.sh", "docker"]
