# Deves
`Hyven frontend tool`  
This project should make it easier to work on the frontend, with commands tooling and debugging pre-installed.
--- 

## Usage
Using deves should be as straight forward as it can be.  
Go to your front-end folder and run 
`docker run -it --rm -p 8080:8080 -p 24678:24678 --mount "type=bind,src=${PWD},dst=/deves/local" hyven/deves --nh --ng dev`
### Params
**`-it`** Makes it so you can interact with the console  
**`-rm`** The container removes itself after usage
**`-p 8080:8080`** It uses the port 8080 of the host and maps it to port 8080 of the container. used for http  
**`-p 24678:24678`** Same as above for port 24678, but used for websocket
**`--mount "type=bind,src=${PWD},dst=/deves/local"`** Makes it that the docker container can use the files of the host machine, it maps them to _/deves/local_  
**`hyven/deves`** Container name  
**`--nh`** Short for `--no-help`, doesn't spawn the help pane on startup  
**`--ng`** Short for `--no-ngrok`, doesn't spawn the ngrok pane on startup  
**`<command>`** Command to run, default is `docker` where you can run multiple commands at once  

## Development

### Live development on deves
Open a console at the root of this project and run
`docker run -it --rm -p 8080:8080 -p 24678:24678 --mount "type=bind,src=${PWD},dst=/deves/local" hyven/deves --shell=execute`  
This opens a deves environment inside the container without tmux, now you can run 
`tmux kill-session || cp ./local/scripts/*.sh ./scripts/ && ./scripts/docker-entry.sh docker` 
and you will run the deves environment with local .sh files 

### Scripts
If it's a new script, make sure the file name is kebab-case and ending with .sh extension.  
After you made the file, run `.scripts/internal/fix-base-script.sh` this will add a header and footer to the file.  
Now you can program between the header and the footer, anything above or below will be removed so make sure you stay between the footer and header.

If you work on the header or footer file, keep in minde that the header and footer are run on every script file including docker-entry.sh.  
After you are done, run `.scripts/internal/fix-base-script.sh` to update it in every script.

### Deploy
> :warning: For deploy you will need an auth token for publishing to docker   
  You can still build for local testing

Run `./scripts/build-and-publish-docker.sh`  
This script will try to build the docker container.
