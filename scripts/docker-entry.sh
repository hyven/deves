#!/usr/bin/env bash
# start base.sh header

set -euo pipefail
VERSION="1.0-alpha4"
EXIT_CODE=0

readonly RESET="\e[0m\e[39m"
readonly BOLD="\e[1m"
readonly UNDERLINE="\e[4m"
readonly RED="\e[91m"
readonly GREEN="\e[92m"
readonly YELLOW="\e[93m"
readonly BLUE="\e[94m"
readonly PINK="\e[95m"
readonly AQUA="\e[96m"
readonly WHITE="\e[97m"

TMUX_KILLABLE="true"
SILENT="false"
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" = "--quiet" ] || [ "$ARGUMENT" = "-q" ]; then
    SILENT="true"
  fi
done

print() {
  if [ "$SILENT" = "false" ]; then
    echo -e "$@"
  fi
}

print "${GREEN}Deves version ${YELLOW}${BOLD}$VERSION${RESET}"
print " ${GREEN}Running: ${YELLOW}$0 ${AQUA}${*}${RESET}"
print
# end base.sh header
DEBUG="false"
HELP="true"
NGROK="true"

main() {
  ARGS=()
  NAME="deves"
  MODE=()
  COMMAND="tmux"
  for ARGUMENT in "$@"; do
    case $ARGUMENT in
    --shell=*) # execute / shell / default / docker / tmux
      ARGUMENT="${ARGUMENT#*=}"
      if [ "$ARGUMENT" = "execute" ]; then
        ARGS=()
        COMMAND="shell"
        MODE+=("shell")
      elif [ "$ARGUMENT" = "shell" ]; then
        COMMAND="shell"
      elif [ "$ARGUMENT" = "default" ] || [ "$ARGUMENT" = "docker" ] || [ "$ARGUMENT" = "tmux" ]; then
        COMMAND="tmux"
      else
        print " ${RED}Argument shell =${YELLOW}$ARGUMENT${RED} is not supported${RESET}"
        EXIT_CODE=1
        exit_script
      fi
      shift
      ;;
    -d | --debug)
      DEBUG="true"
      shift
      ;;
    -nh | --no-help)
      HELP="false"
      shift
      ;;
    -ng | --no-ngrok)
      NGROK="false"
      shift
      ;;
    -q | --quiet)
      MODE=("$ARGUMENT" "${MODE[@]}")
      shift
      ;;
    *)
      ARGS+=("$ARGUMENT")
      shift
      ;;
    esac
  done

  print "${GREEN}Starting ${YELLOW}deves${GREEN} in mode ${AQUA}${COMMAND}${RESET}"
  if [ "$COMMAND" = "tmux" ]; then
    start_tmux "${MODE[@]}" "${ARGS[@]}"
  else
    ./scripts/deves.sh "${MODE[@]}" "${ARGS[@]}"
  fi
}

start_tmux() {
  tmux new-session -s "$NAME" -d ./scripts/deves.sh "$@"
  tmux set-option -t "$NAME" -w mouse on
  if [ $DEBUG == "true" ]; then
    tmux set-option -t "$NAME" -w remain-on-exit failed
  fi
  if [ $NGROK == "true" ]; then
    tmux split-window -t "$NAME" -h -l 10 ./tools/ngrok http --region=eu 8080
    tmux set-option -t "$NAME" -p other-pane-height 10
    tmux set-option -t "$NAME" -p other-pane-width 63
  fi
  if [ $HELP == "true" ]; then
    tmux split-window -t "$NAME" -v ./scripts/deves-help-panel.sh -x
    tmux resize-pane -t "{right}" -x 63
  fi
  if [ $NGROK == "true" ] || [ $HELP == "true" ]; then
    tmux set-hook -t "$NAME" client-resized "resize-pane -x 63 -t {right}"
  fi
  tmux select-pane -t "$NAME" -L
  tmux -2 attach-session -t "$NAME"
}
# start base.sh footer

exit_script() {
  print "${PINK}EXIT: ${EXIT_CODE}${RESET}"
  if [[ -n $(pgrep tmux) ]] && [ "$TMUX_KILLABLE" = "true" ]; then
    tmux kill-session
  else
    exit $EXIT_CODE
  fi
}

main "$@"
exit_script
# end base.sh footer
