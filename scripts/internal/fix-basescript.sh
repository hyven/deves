#!/usr/bin/env bash

main() {
  fix-header-footer-scripts
  make-scripts-executable
}

fix-header-footer-scripts() {
  echo "Adding header and footer to all scripts"
  for f in scripts/*.sh; do
    echo "  - ${f}"
    cat ./scripts/internal/base.sh-header-template >"${f}.tmp"
    echo >>"${f}.tmp"
    awk '/# end base.sh header/{p++;if(p==1){next}}p' $f | awk '/# start base.sh footer/{exit}1' | awk '{ sub("\r$", ""); print }' >>"${f}.tmp"
    cat ./scripts/internal/base.sh-footer-template >>"${f}.tmp"
    mv "${f}.tmp" $f
  done
  echo " Done"
}

make-scripts-executable() {
  echo "Fixing .sh files to be executable"
  chmod -v +x ./scripts/*.sh
  echo " Fixed"
}

main
