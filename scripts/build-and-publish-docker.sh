#!/usr/bin/env bash
# start base.sh header

set -euo pipefail
VERSION="1.0-alpha4"
EXIT_CODE=0

readonly RESET="\e[0m\e[39m"
readonly BOLD="\e[1m"
readonly UNDERLINE="\e[4m"
readonly RED="\e[91m"
readonly GREEN="\e[92m"
readonly YELLOW="\e[93m"
readonly BLUE="\e[94m"
readonly PINK="\e[95m"
readonly AQUA="\e[96m"
readonly WHITE="\e[97m"

TMUX_KILLABLE="true"
SILENT="false"
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" = "--quiet" ] || [ "$ARGUMENT" = "-q" ]; then
    SILENT="true"
  fi
done

print() {
  if [ "$SILENT" = "false" ]; then
    echo -e "$@"
  fi
}

print "${GREEN}Deves version ${YELLOW}${BOLD}$VERSION${RESET}"
print " ${GREEN}Running: ${YELLOW}$0 ${AQUA}${*}${RESET}"
print
# end base.sh header
CONTAINER=hyven/deves

main() {
  build_image
  publish_build
}

build_image() {
  echo -e "${GREEN}Building...${RESET}"
  if docker build -t ${CONTAINER} .; then
    echo -e "${GREEN}Done building main file${RESET}"
  else
    echo -e "${RED}Failed building main file${RESET}"
    EXIT_CODE=1
  fi
  if docker build -t ${CONTAINER}:shell -f shell.Dockerfile .; then
      echo -e "${GREEN}Done building shell${RESET}"
    else
      echo -e "${RED}Failed building shell${RESET}"
      EXIT_CODE=1
    fi
}

publish_build() {
  echo -e "${GREEN}Pushing...${RESET}"
  if docker push --all-tags ${CONTAINER} && docker push ${CONTAINER}:shell; then
    echo -e "${GREEN}Pushed all${RESET}"
  else
    echo -e "${RED}Failed pushing${RESET}"
    EXIT_CODE=1
  fi
}

# start base.sh footer

exit_script() {
  print "${PINK}EXIT: ${EXIT_CODE}${RESET}"
  if [[ -n $(pgrep tmux) ]] && [ "$TMUX_KILLABLE" = "true" ]; then
    tmux kill-session
  else
    exit $EXIT_CODE
  fi
}

main "$@"
exit_script
# end base.sh footer
