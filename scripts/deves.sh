#!/usr/bin/env bash
# start base.sh header

set -euo pipefail
VERSION="1.0-alpha4"
EXIT_CODE=0

readonly RESET="\e[0m\e[39m"
readonly BOLD="\e[1m"
readonly UNDERLINE="\e[4m"
readonly RED="\e[91m"
readonly GREEN="\e[92m"
readonly YELLOW="\e[93m"
readonly BLUE="\e[94m"
readonly PINK="\e[95m"
readonly AQUA="\e[96m"
readonly WHITE="\e[97m"

TMUX_KILLABLE="true"
SILENT="false"
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" = "--quiet" ] || [ "$ARGUMENT" = "-q" ]; then
    SILENT="true"
  fi
done

print() {
  if [ "$SILENT" = "false" ]; then
    echo -e "$@"
  fi
}

print "${GREEN}Deves version ${YELLOW}${BOLD}$VERSION${RESET}"
print " ${GREEN}Running: ${YELLOW}$0 ${AQUA}${*}${RESET}"
print
# end base.sh header
DOCKER=0

run_docker() {
  DOCKER=1
  if [ $# -gt 0 ]; then
    print "${GREEN}Command: ${BLUE}$*${RESET}"
    main "$@"
  fi
  while true; do
    echo -e -n "${GREEN}Command: ${BLUE}"
    read -r -a cmd
    echo -e -n "${RESET}"
    if [ "${cmd[*]}" == "" ] || [ "${cmd[*]}" == "docker" ]; then
      main help
      continue
    fi
    main "${cmd[@]}"
  done
}

run_shell_at() {
  NEW_PATH="/deves/scripts:$PATH:/deves/local:/deves:/deves/tools"
  CURRENT_PATH="$PWD"
  cd "$1"
  set +euo pipefail
  if [ $# == 1 ]; then
    env PATH="$NEW_PATH" "$SHELL"
  else
    env PATH="$NEW_PATH" "${@:2}"
  fi
  set -euo pipefail
  EXIT_CODE=$?
  cd "$CURRENT_PATH"
}

run_shell() {
  run_shell_at "." "$@"
}

print_help() {
  local DOCKER_HELP=""
  if [ $DOCKER == 0 ]; then
    DOCKER_HELP="\n  ${BLUE}docker${RESET}         ${GREEN}Run multiple commands after each other${RESET}\n"
  else
    DOCKER_HELP="\n  ${BLUE}exit${RESET}           ${GREEN}Exit out of the docker loop${RESET}\n"
  fi
  local SCRIPTS=$(ls -p scripts/ | grep -v / | tr '\n' ' ')
  local TOOLS=$(ls -p tools/ | grep -v / | tr '\n' ' ')
  echo -e "$(
    cat <<HELP
${GREEN}Usage:${RESET} ${YELLOW}$0${RESET} ${BLUE}<command>${RESET} ${AQUA}[...arguments]${RESET}

${GREEN}Available commands:${RESET}
  ${BLUE}build${RESET}          ${GREEN}Builds npm site.${RESET}
  ${BLUE}debug${RESET}          ${GREEN}Does something.${RESET}
  ${BLUE}dev${RESET}            ${GREEN}Starts npm development session.${RESET}
  ${BLUE}help${RESET}           ${GREEN}Lists this page.${RESET}
  ${BLUE}shell${RESET}          ${GREEN}Starts a shell.${RESET}
  ${BLUE}version${RESET}        ${GREEN}Prints the version of this script.${RESET}
${DOCKER_HELP}
${GREEN}The following commands are available when you use ${BLUE}${BOLD}shell${GREEN}:${RESET}
 ${AQUA}${SCRIPTS}${RESET} ${BLUE}${TOOLS}${RESET}
HELP
  )"

}

print_version() {
  echo -e "${GREEN}Version: ${YELLOW}${BOLD}$VERSION${RESET}"
}

main() {
  if [ $# == 0 ]; then
    print
    print "${RED}${BOLD}Use an argument!${RESET}"
    EXIT_CODE=10
  else
    for argument in "$@"; do
      case $argument in
      build)
        run_shell_at "/deves/local" npm i
        run_shell_at "/deves/local" npm run build "${@:2}"
        break
        ;;
      debug)
        print "${UNDERLINE}7h12 12 A 5uP3r 53CR37 m355A93${RESET}"
        break
        ;;
      dev)
        run_shell_at "/deves/local" npm i
        run_shell_at "/deves/local" npm run dev "${@:2}"
        break
        ;;
      docker)
        run_docker "${@:2}"
        break
        ;;
      help)
        print_help
        shift
        ;;
      shell | sh)
        run_shell "${@:2}"
        break
        ;;
      version)
        print_version
        shift
        ;;
      exit)
        exit_script
        break
        ;;
      -*)
        parse_argument "$argument"
        shift
        ;;
      *)
        echo
        print "${RED}${BOLD}Unknown argument ${YELLOW}${BOLD}$argument ${*:2}${RESET}"
        print_help
        EXIT_CODE=1
        break
        ;;
      esac
    done
  fi
}

parse_argument() {
  case $argument in
  -q | --quiet)
    SILENT="true"
    ;;
  -v | --verbose)
    SILENT="false"
    ;;
  -x | --exit-tmux)
    TMUX_KILLABLE="false"
    ;;
  *)
    print "${RED}Argument not parsed: ${YELLOW}$*${RESET}"
    ;;
  esac
}
# start base.sh footer

exit_script() {
  print "${PINK}EXIT: ${EXIT_CODE}${RESET}"
  if [[ -n $(pgrep tmux) ]] && [ "$TMUX_KILLABLE" = "true" ]; then
    tmux kill-session
  else
    exit $EXIT_CODE
  fi
}

main "$@"
exit_script
# end base.sh footer
