#!/usr/bin/env bash
# start base.sh header

set -euo pipefail
VERSION="1.0-alpha4"
EXIT_CODE=0

readonly RESET="\e[0m\e[39m"
readonly BOLD="\e[1m"
readonly UNDERLINE="\e[4m"
readonly RED="\e[91m"
readonly GREEN="\e[92m"
readonly YELLOW="\e[93m"
readonly BLUE="\e[94m"
readonly PINK="\e[95m"
readonly AQUA="\e[96m"
readonly WHITE="\e[97m"

TMUX_KILLABLE="true"
SILENT="false"
for ARGUMENT in "$@"; do
  if [ "$ARGUMENT" = "--quiet" ] || [ "$ARGUMENT" = "-q" ]; then
    SILENT="true"
  fi
done

print() {
  if [ "$SILENT" = "false" ]; then
    echo -e "$@"
  fi
}

print "${GREEN}Deves version ${YELLOW}${BOLD}$VERSION${RESET}"
print " ${GREEN}Running: ${YELLOW}$0 ${AQUA}${*}${RESET}"
print
# end base.sh header
main() {
  TMUX_KILLABLE="false"
  clear
  echo -e "${GREEN}Cheat sheet for ${YELLOW}deves${RESET}"
  echo
  echo -e "$(
    cat <<HELP
${PINK}Console hotkeys${RESET}: ${YELLOW}Crtl${RESET}+${YELLOW}B${RESET}+${YELLOW}?${RESET}     Show help for tmux

HELP
  )"
  ./scripts/deves.sh -x -q help
  sleep 2h
}
# start base.sh footer

exit_script() {
  print "${PINK}EXIT: ${EXIT_CODE}${RESET}"
  if [[ -n $(pgrep tmux) ]] && [ "$TMUX_KILLABLE" = "true" ]; then
    tmux kill-session
  else
    exit $EXIT_CODE
  fi
}

main "$@"
exit_script
# end base.sh footer
